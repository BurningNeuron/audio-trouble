Unity 3D Audio Recording and Playback Sample
============================================

This project consists of a a simple UI consisting of three buttons:
* Push to record / release to stop recording
* Playback
* Save audio to wav (uses SavWav taken from https://gist.github.com/darktable/2317063)

Pushing and holding the record button records the default microphone input to an AudioClip.

After recording, pressing the Playback button will play the last recording.

2017-04-15 23:45 AEST UPDATE
----------------------------
When I started to make a screen recording to show the code in this project
and the garbled audio I was surprised to find that the audio was no longer garbled.

Turns out it is only garbled when using the internal microphone of the Macbook Pro.


Garbled Audio On Mac Bug
------------------------
Audio on Mac OSX 10.11.16 is garbled and "too fast". This happens in both the editor and compiled.

Compiled for Android works as expected without garbled audio

Compiled for Windows works as expected without garbled audio

Not tested on other version on of OSX

Not tested on iOS

Bug appears for both Unity 5.5.0f3 and 5.6.0f3

Not tested with other versions of Unity.

Contact
-------
* Michael McHugh (michael@burningneuorn.io)
* https://twitter.com/BurningNeuronIO
* https://www.codementor.io/burningneuron
* https://clarity.fm/burningneuron
* https://linked.in/in/mrmchugh
