﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTestingController : MonoBehaviour {

    [Header("Settings")]
    [SerializeField] int recordingLength = 60;
    [SerializeField] int frequency = 16000;
    [SerializeField] string saveFileName = "sample.wav";

    [Header("Wired Components")]
    [SerializeField] AudioSource audioSource;

    AudioClip recordedClip;

    public void StartRecording() {
        Debug.Log("Start Recording");
        recordedClip = Microphone.Start(null, false, recordingLength, frequency);
    }

    public void EndRecording() {
        Debug.Log("End Recording");
        Microphone.End(null);
    }

    public void PlaybackRecording() {
        Debug.Log("Playback Recording");
        audioSource.PlayOneShot(recordedClip);
    }

    public void SaveAsWavFile() {
        SavWav.Save(saveFileName, recordedClip);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
